package ru.tsc.gulin.tm.exception.system;

import ru.tsc.gulin.tm.exception.AbstractException;

public class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Error! Login or password is incorrect...");
    }

}
